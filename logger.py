import random

from tensorboardX import SummaryWriter
from plotting_utils import plot_alignment_to_numpy


class TransliteratorLogger(SummaryWriter):
    def __init__(self, logdir):
        super(TransliteratorLogger, self).__init__(logdir)

    def log_training(self, reduced_loss, grad_norm, learning_rate, duration,
                     iteration):
        self.add_scalar("training.loss", reduced_loss, iteration)
        self.add_scalar("grad.norm", grad_norm, iteration)
        self.add_scalar("learning.rate", learning_rate, iteration)
        self.add_scalar("duration", duration, iteration)

    def log_validation(self, ler, per, model, iteration):
        self.add_scalar("validation.ler", ler, iteration)
        self.add_scalar("validation.per", per, iteration)
        # plot distribution of parameters
        for tag, value in model.named_parameters():
            tag = tag.replace('.', '/')
            try:
                self.add_histogram(tag, value.data.cpu().numpy(), iteration)
            except Exception as e:
                print(tag, value.data.cpu().numpy())
                raise e
