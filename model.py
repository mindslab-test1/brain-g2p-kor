import torch
import torch.nn as nn

from data import kor_symbol_boundary


class LinearNorm(nn.Module):
    def __init__(self, in_dim, out_dim, bias=True, w_init_gain='linear'):
        super(LinearNorm, self).__init__()
        self.linear_layer = nn.Linear(in_dim, out_dim, bias=bias)

        nn.init.xavier_uniform_(
            self.linear_layer.weight,
            gain=nn.init.calculate_gain(w_init_gain))

    def forward(self, x):
        return self.linear_layer(x)


class EncoderRNN(nn.Module):
    def __init__(self, input_size, hidden_size):
        super(EncoderRNN, self).__init__()

        self.embedding = nn.Embedding(input_size, hidden_size)
        self.gru = nn.GRU(hidden_size, hidden_size // 2, batch_first=True, bidirectional=True)

    def forward(self, inputs, input_lengths):
        embedded = self.embedding(inputs)

        embedded = nn.utils.rnn.pack_padded_sequence(embedded, input_lengths, batch_first=True)
        self.gru.flatten_parameters()
        outputs, hidden = self.gru(embedded)

        outputs, _ = nn.utils.rnn.pad_packed_sequence(outputs, batch_first=True)

        return outputs

    def inference(self, inputs):
        embedded = self.embedding(inputs)

        self.gru.flatten_parameters()
        outputs, _ = self.gru(embedded)

        return outputs


class Transliterator(nn.Module):
    def __init__(self, hparams):
        super(Transliterator, self).__init__()
        self.encoder = EncoderRNN(hparams.input_size, hparams.hidden_size)
        self.out_layer = LinearNorm(hparams.hidden_size, hparams.output_size)

        self.transition_mask = nn.Parameter(
            data=torch.full((hparams.output_size, hparams.output_size), 1, dtype=torch.uint8),
            requires_grad=False
        )
        for idx, (in_boundary_s, in_boundary_e) in enumerate(kor_symbol_boundary):
            if idx == 0:
                for in_boundary_idx in range(in_boundary_s, in_boundary_e):
                    self.transition_mask[in_boundary_idx, in_boundary_idx] = 0
            else:
                self.transition_mask[in_boundary_s:in_boundary_e, in_boundary_s:in_boundary_e] = 0
        # self.encoder.embedding.weight = self.out.linear_layer.weight

    def forward(self, inputs, input_lengths=None):
        if input_lengths is None:
            encoder_outputs = self.encoder.inference(inputs)
        else:
            encoder_outputs = self.encoder(inputs, input_lengths)
        output = self.out_layer(encoder_outputs)
        transition_mask = torch.index_select(self.transition_mask, 0, inputs.view(-1))
        transition_mask = transition_mask.view(inputs.size(0), inputs.size(1), -1)
        output.masked_fill_(transition_mask, -float("inf"))
        return output

    def inference(self, inputs, input_lengths=None):
        output = self.forward(inputs, input_lengths)
        _, output = output.topk(1, dim=-1)
        output = output.squeeze(-1)
        return output
