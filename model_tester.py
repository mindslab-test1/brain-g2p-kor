import torch
import argparse


from data import sequence_to_kor, korean_cleaners
from train import load_model


def preprocess(word_list):
    word_list = [torch.cuda.LongTensor(korean_cleaners(text)) for text in word_list]
    input_lengths, ids_sorted_decreasing = torch.sort(
        torch.cuda.LongTensor([len(text) for text in word_list]),
        dim=0, descending=True)
    max_input_len = input_lengths[0]

    input_padded = torch.cuda.LongTensor(len(word_list), max_input_len)
    input_padded.zero_()
    for i in range(len(ids_sorted_decreasing)):
        text = word_list[ids_sorted_decreasing[i]]
        input_padded[i, :text.size(0)] = text
    output_ids = torch.sort(ids_sorted_decreasing)[1]
    return (input_padded, input_lengths), output_ids


class Kg2p(object):
    def __init__(self, checkpoint_path, device):
        super().__init__()
        torch.cuda.set_device(device)
        self.device = device

        checkpoint = torch.load(checkpoint_path, map_location='cpu')
        hparams = checkpoint['hparams']

        state_dict = checkpoint['state_dict']

        self.model = load_model(hparams)
        self.model.load_state_dict(state_dict)
        self.model.eval()

        del hparams
        del state_dict
        del checkpoint
        torch.cuda.empty_cache()

    def transliterate(self, korean_g):
        torch.cuda.set_device(self.device)
        with torch.no_grad():
            korean_g, output_ids = preprocess(korean_g)
            korean_p = self.model.inference(*korean_g)
            korean_p = [sequence_to_kor(korean_p[idx.item()]) for idx in output_ids]

            return korean_p


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Korean G2P Tester')
    parser.add_argument('-m', '--model',
                        nargs='?',
                        dest='model',
                        required=True,
                        help='Model Path.',
                        type=str)

    parser.add_argument('-d', '--device',
                        nargs='?',
                        dest='device',
                        help='gpu device',
                        type=int,
                        default=0)

    args = parser.parse_args()

    g2pK = Kg2p(args.model, args.device)

    # import re
    #
    # pattern_dict = {'갈등':'갈뜽', '갈증':'갈쯩', '갉아':'갈가', '값어치':'가버치', '값있는':'가빈는', '강가':'강까', '강요':'강요', '강줄기':'강쭐기', '같아':'가타', '같애':'가태',
    #                 '검열':'검녈', '것이':'거시', '것인가':'거신가', '것인데':'거신데', '겉옷':'거돋', '결단력':'결딴녁', '결막염':'결망념', '결실과':'결씰과', '결정':'결쩡',
    #                 '경영':'경영', '고갯짓':'고개찓', '곳곳에':'곧꼬세', '공권력':'공꿘녁', '구근류':'구근뉴', '국민윤리':'궁민뉼리', '굴속':'굴쏙', '굴옆':'굴렾', '그믐달':'그믐딸', '글자':'글짜',
    #                 '글재주':'글째주', '금융':'금늉', '기권':'기꿘', '길가':'길까', '김밥':'김빱', '깃발':'기빨', '깻잎':'깬닙', '껴안다':'껴안따', '껴안지':'껴안찌', '꽃이':'꼬치', '군요':'군뇨',
    #                 '꽃잎':'꼰닙', '끝없이':'끄덥씨', '나뭇잎':'나문닢', '날개':'날개', '낡은':'날근', '남존여비':'남존녀비', '낯익다':'난닉따', '낯익은':'난니근', '내복약':'내봉냑', '냇가':'내까',
    #                 '넋없다':'너겁따', '넓고':'넙꼬', '넓덩글':'넙떵글', '넓둥글다':'넙뚱글다', '넓죽':'넙쭉', '높이':'노피', '놓치':'녿치', '놓치다':'녿치다', '눈대중':'눈때중', '눈동자':'눈똥자',
    #                 '눈요기':'눈뇨기', '늑막염':'능망념', '늦여름':'는녀름', '늦은':'느즌', '달성':'달썽', '담배값':'담배깝', '담고':'담꼬', '담요':'담뇨', '당신에':'당시네', '당연':'당연', '대가':'대까',
    #                 '대팻밥':'대패빱', '더듬지':'더듬찌', '덧붙였다':'덛뿌쳗따', '도리깻열':'도리깬녈', '동양':'동양', '동원령':'동원녕', '될지':'될찌', '뒷얘기':'뒨냬기', '뒷윷':'뒨뉻', '드물지':'드물지',
    #                 '든요':'든뇨', '들새':'들쌔', '들여':'드려', '들일':'들릴', '듯이':'드시', '등불':'등뿔', '등용문':'등용문', '디귿에':'디그세', '디귿이':'디그시', '막일':'망닐', '만의':'마네', '많고':'만코',
    #                 '많다':'만타', '맏형':'마텽', '말살':'말쌀', '말야':'마랴', '맛없다':'마덥따', '맛있다':'마싣따', '맞어':'마저', '맨입':'맨님', '맺어':'매저', '먹은엿':'머근녇', '먹을엿':'머글렫',
    #                 '멋있다':'머싣따', '명예':'명예', '몰두':'몰뚜', '몰상식':'몰쌍식', '무기권':'무기꿘', '무엇인가':'무어신가', '문고리':'문꼬리', '문법':'문뻡', '물가':'물까', '물결은':'물껴른', '물고기':'물꼬기',
    #                 '물동이':'물똥이', '물속':'물쏙', '물약':'물략', '물엿': '물렷', '물옆': '물렾', '물줄기': '물쭐기', '물증': '물쯩', '물질': '물찔', '묽고': '물꼬', '민증': '민쯩', '바람결': '바람껼',
    #                 '밖에':'바께', '밖으로':'바끄로', '발달':'발딸', '발동':'발똥', '발바닥':'발빠닥', '발전':'발쩐', '밝혔다':'발켣따', '밝히':'발키', '밝히는':'발키는', '밝히다':'발키다', '밟고':'밥꼬',
    #                 '밟는':'밥는', '밟다':'밥따', '밤윳':'밤뉻', '뱃머리':'밴머리', '뱃속':'배쏙', '뱃전':'배쩐', '베갯잇':'베갠닛', '별도':'별또', '보문로':'보문노', '볶은엿':'보끈녇', '볼일':'볼릴',
    #                 '불법':'불뻡', '불빛':'불삗', '불세출': '불쎄출', '불소': '불쏘', '불여우': '불려우', '불연속': '부련속', '붙여': '부쳐', '붙여서': '부쳐서', '빛이': '비치', '빨간색': '빨간색',
    #                 '빨랫돌':'빨래똘', '사건':'사껀', '삯일':'상닐', '산새':'산쌔', '삼고':'삼꼬', '삼다':'삼따', '삼대':'삼대', '삼십일여간':'삼시빌려간', '삼자':'삼짜', '상견례':'상견녜', '색연필':'생년필',
    #                 '샛길':'새낄', '생산량':'생산냥', '생산력':'생산녁', '서른여섯':'서른녀섣', '서울역':'서울력', '섞여':'석껴', '섞으며':'서끄며', '선생':'선생', '선생님':'선생님', '설득을':'설뜨글', '설익다':'설릭따',
    #                 '성격':'성껵', '세분':'세분', '소유권':'소유꿘', '소증':'소쯩', '손가락':'손까락', '손바닥':'손빠닥', '손재주':'손째주', '솔잎':'솔립', '솜이불':'솜니불', '송별연':'송벼련', '술독':'술똑', '술병':'술뼝',
    #                 '술잎':'술립', '술자리':'술짜리', '술잔': '술짠', '스물여섯': '스물려섣', '식용유': '시굥뉴', '신가': '신가', '신경': '싄경', '신고': '싱고', '신기': '싄기', '신다': '신따',
    #                 '신문로':'신문노', '신바람':'신빠람', '신여성':'신녀성', '신자':'신짜', '신중한':'신중한', '실제':'실쩨', '싫증':'실쯩', '쑥갓요':'쑥간뇨', '아랫니':'아랜니', '아침밥':'아침빱', '안고':'안꼬', '안팎에':'안파께',
    #                 '않고':'안코', '않다':'안타', '약의':'야긔', '알 수 있고':'알 쑤 읻꼬', '알맞은':'알마즌', '앞이':'아피', '야금야금':'야금냐금', '약간에':'약까네', '없는':'업는', '엮어':'역꺼', '열정':'열쩡', '영업용':'영엄뇽', '영역을':'영여글',
    #                 '오천원':'오처넌', '옷입는':'온닙는', '옷입다':'온닙따','잇을':'이슬','있을':'읻쓸', '요건':'요껀', '욜랑욜랑':'욜랑뇰랑', '용법':'용뻡', '원색':'원색', '유권':'유꿘', '유들유들':'유들류들', '유세부터':'유세부터', '윤기':'윤끼', '응용':'응용', '의견란':'의견난',
    #                 '이밖에':'이바께', '이원론':'이원논', '이죽이죽':'이중니죽', '인격':'인껵', '인기':'인끼', '일단':'일딴', '일대':'일때', '일수':'일쑤', '일시':'일씨', '일절':'일쩔', '일정':'일쩡', '일주기':'일쭈기', '임진란':'임진난',
    #                 '입원료':'이붠뇨', '잘입다': '잘립따', '잠자리': '잠짜리', '전세방': '전세빵', '전세집': '전세찝', '절대': '절때', '절도': '절또', '절실': '절씰', '절실한': '절씰한', '젊지': '점찌', '점심밥': '점심빱', '정권': '정꿘',
    #                 '주의':'주이', '젖어미':'저더미', '젖을':'저즐', '조건':'조껀', '줄게요':'줄께요', '줄넘기':'줄럼끼', '중요':'중요', '증권':'증꿘', '지읒에':'지으세', '직원':'지권', '직행열차':'지캥녈차', '집안일을':'지반니를', '쫒아':'쪼차',
    #                 '찢어':'찌저', '차이점이':'차이쩌미', '참여':'차며', '창가':'창까', '창살':'창쌀', '찾아':'차자', '찾아내야':'차자내야', '찾아야':'차자야', '찾을':'차즐', '철도':'철또', '철저':'철쩌', '첫인상':'처딘상', '초승달':'초승딸',
    #                 '촬영':'촤령', '촬영장':'촤령장', '출세':'출쎄', '출신':'출씬', '출연':'추련', '치읓이':'치으세', '콧날':'콘날', '콧등':'코뜽', '콩엿':'콩녇', '키읔이':'키으기', '탄생':'탄생', '탐욕':'타묙', '탓인가':'타신가', '통증':'통쯩',
    #                 '툇마루':'퇸마루', '파란색':'파란색', '평가':'평까', '피읖에':'피으베', '필연':'피련', '필요':'피료', '필자':'필짜', '하나님의':'하나니메', '한여름':'한녀름', '한일':'한닐', '할걸':'할껄', '할밖에':'할빠께', '할세라':'할쎄라',
    #                 '할수록':'할쑤록', '할일':'할릴', '할지라도':'할찌라도', '할지언정':'할찌언정', '할진대':'할찐대', '함양':'하먕', '함유':'하뮤', '헌신적인':'헌신저긴', '헛웃음':'허두슴', '홑이불':'혼니불', '화학약품에':'화항냑푸메', '활동':'활똥',
    #                 '활용':'화룡', '횡단로':'횡단노', '효과':'효꽈', '휘발유':'휘발류'}
    #
    # pattern_list = ['갈등', '갈증', '갉아', '값어치', '값있는', '강가', '강요', '강줄기', '같아', '같애', '검열', '것이', '것인가', '것인데', '겉옷', '결단력', '결막염', '결실과', '결정', '경영', '고갯짓', '곳곳에', '공권력', '구근류', '군요',
    #                 '국민윤리', '굴속', '굴옆', '그믐달', '글자', '글재주', '금융', '기권', '길가', '김밥', '깃발', '깻잎', '껴안다', '껴안지', '꽃이', '꽃잎', '끝없이', '나뭇잎', '날개', '낡은', '남존여비', '낯익다', '낯익은', '내복약', '냇가',
    #                 '넋없다', '넓고', '넓덩글', '넓둥글다', '넓죽', '높이', '놓치', '놓치다', '눈대중', '눈동자', '눈요기', '늑막염', '늦여름', '늦은', '달성', '담배값', '담고', '담요', '당신에', '당연', '대가', '대팻밥', '더듬지', '덧붙였다', '도리깻열',
    #                 '동양', '동원령', '될지', '뒷얘기', '뒷윷', '드물지', '든요', '들새', '들여', '들일', '듯이', '등불', '등용문', '디귿에', '디귿이', '막일', '만의', '많고', '많다', '맏형', '말살', '말야', '맛없다', '맛있다', '맞어', '맨입', '맺어',
    #                 '먹은엿', '먹을엿', '멋있다', '명예', '몰두', '몰상식', '무기권', '무엇인가', '문고리', '문법', '물가', '물결은', '물고기', '물동이', '물속', '물약', '물엿', '물엿', '물옆', '물줄기', '물증', '물질', '묽고', '민증', '바람결', '밖에', '밖으로',
    #                 '발달', '발동', '발바닥', '발전', '밝혔다', '밝히', '밝히는', '밝히다', '밟고', '밟는', '밟다', '밤윳', '뱃머리', '뱃속', '뱃전', '베갯잇', '별도', '보문로', '볶은엿', '볼일', '불법', '불빛', '불세출', '불세출', '불소', '불여우', '불연속',
    #                 '붙여', '붙여서', '빛이', '빨간색', '빨랫돌', '사건', '삯일', '산새', '삼고', '삼다', '삼대', '삼십일여간', '삼자', '상견례', '색연필', '샛길', '생산량', '생산력', '서른여섯', '서울역', '섞여', '섞으며', '선생', '선생님', '설득을', '설익다',
    #                 '성격', '세분', '소유권', '소증', '손가락', '손바닥', '손재주', '솔잎', '솜이불', '송별연', '술독', '술병', '술잎', '술자리', '술잔', '술잔', '스물여섯', '스물여섯', '식용유', '신가', '신경', '신고', '신기', '신다', '신문로', '신바람', '신여성', '신자',
    #                 '신중한', '실제', '싫증', '쑥갓요', '아랫니', '잇을','있을','약의', '아침밥', '안고', '안팎에', '않고', '않다', '알 수 있고', '알맞은', '앞이', '야금야금', '약간에', '없는', '엮어', '열정', '영업용', '영역을', '오천원', '옷입는', '옷입다', '요건', '욜랑욜랑', '용법', '원색',
    #                 '유권', '유들유들', '유세부터', '윤기', '응용', '의견란', '이밖에', '이원론', '이죽이죽', '인격', '인기', '일단', '일대', '일수', '일시', '일절', '일정', '일주기', '임진란', '입원료', '잘입다', '잘입다', '잠자리', '전세방', '전세집', '절대', '절도',
    #                 '절실', '절실한', '젊지', '점심밥', '정권', '주의', '젖어미', '젖을', '조건', '줄게요', '줄넘기', '중요', '증권', '지읒에', '직원', '직행열차', '집안일을', '쫒아', '찢어', '차이점이', '참여', '창가', '창살', '찾아', '찾아내야', '찾아야', '찾을',
    #                 '철도', '철저', '첫인상', '초승달', '촬영', '촬영장', '출세', '출신', '출연', '치읓이', '콧날', '콧등', '콩엿', '키읔이', '탄생', '탐욕', '탓인가', '통증', '툇마루', '파란색', '평가', '피읖에', '필연', '필요', '필자', '하나님의', '한여름', '한일',
    #                 '할걸', '할밖에', '할세라', '할수록', '할일', '할지라도', '할지언정', '할진대', '함양', '함유', '헌신적인', '헛웃음', '홑이불', '화학약품에', '활동', '활용', '횡단로', '효과', '휘발유']
    #
    # pattern_dict = {'갈등': '갈뜽'}
    # pattern_list = ['갈등']
    while True:
        print('====================================')
        text = input('Korean G2P INPUT:\n')
        text2 = text
        # for i in range(len(pattern_list)):
        #     text2 = re.sub(pattern=pattern_list[i], repl=pattern_dict[pattern_list[i]], string=text2)
        if text == 'end':
            break
        else:
            x = g2pK.transliterate([text2])
            print('------------------------------------')
            print('Korean G2P OUTPUT:\n', str(x[0]))
            print('====================================')
