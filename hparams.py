import tensorflow as tf

from data import kor_symbols


def create_hparams(hparams_string=None, verbose=False):
    hparams = tf.contrib.training.HParams(
        ################################
        # Experiment Parameters        #
        ################################
        epochs=250000,
        iters_per_checkpoint=500,
        seed=1234,
        cudnn_enabled=True,
        cudnn_benchmark=False,
        ################################
        # Data Parameters             #
        ################################
        training_data='dataset_train.txt',
        validation_data='dataset_val.txt',
        ################################
        # Model Parameters             #
        ################################
        input_size=len(kor_symbols),
        hidden_size=512,
        output_size=len(kor_symbols),
        ################################
        # Optimization Hyperparameters #
        ################################
        use_saved_learning_rate=False,
        learning_rate=1e-3,
        weight_decay=1e-6,
        grad_clip_thresh=1.0,
        batch_size=128
    )

    if hparams_string:
        tf.logging.info('Parsing command line hparams: %s', hparams_string)
        hparams.parse(hparams_string)

    if verbose:
        tf.logging.info('Final parsed hparams: %s', hparams.values())

    return hparams
